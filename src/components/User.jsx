import React from 'react';
import {useSelector} from 'react-redux'
import {selectUser} from '../redux/userSlice'
const User = () =>{
    const {name,isLogged} = useSelector(selectUser)
    return(
        <div>
            <h1>Usuario: {name} {isLogged}</h1>
        </div>
    )
}

export default User;